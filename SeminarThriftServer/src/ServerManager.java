import org.apache.thrift.TException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ServerManager implements SeminarManager.Iface {

    private Map<String, Seminar> seminare;

    ServerManager() {
        this.seminare = new HashMap<>();
    }

    @Override
    public void addParticipantToSeminar(Seminar seminar, Participant teilnehmer) throws TException {
        if (seminar.participants == null) {
            seminar.participants = new ArrayList<>();
        }
        seminar.participants.add(teilnehmer);
        if (!seminare.containsKey(seminar.name)) {
            seminare.put(seminar.name, seminar);
        }
        System.out.println("Participant added");
    }

    @Override
    public Seminar getSeminarByName(String name) throws InvalidOperation, TException {
        var seminar = seminare.get(name);
        if (seminar == null) {
            throw new InvalidOperation(-1, "Seminar does not exist");
        }
        System.out.printf("Returned seminar%s%n", seminar.name);
        return seminar;
    }
}
