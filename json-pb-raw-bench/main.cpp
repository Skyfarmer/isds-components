#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <memory>
#include <chrono>

#include "json.hpp"

#include "seminar.pb.h"

constexpr int iterations = 1000000;


struct seminar {
    int id{};
    std::string name;
    int num_participants{};
    bool summer_term{};
};


void test_raw(const seminar &_seminar) {
    std::vector<seminar> data;
    data.reserve(iterations);
    for (auto i = 0; i < iterations; i++) {
        data.push_back(_seminar);
    }
    std::for_each(data.begin(), data.end(), [&](const seminar &s) {
        auto new_seminar = s;
        if (new_seminar.id != _seminar.id) {
            std::cout << "ERROR!!!" << std::endl;
        }
    });
}

void to_json(nlohmann::json &j, const seminar &p) {
    j = nlohmann::json{{"id",               p.id},
                       {"name",             p.name},
                       {"num_participants", p.num_participants},
                       {"summer_term",      p.summer_term}};
}

void from_json(const nlohmann::json &j, seminar &p) {
    j.at("id").get_to(p.id);
    j.at("name").get_to(p.name);
    j.at("num_participants").get_to(p.num_participants);
    j.at("summer_term").get_to(p.summer_term);
}

void test_json(const seminar &_seminar) {
    std::vector<nlohmann::json> data;
    data.reserve(iterations);
    for (auto i = 0; i < iterations; i++) {
        data.emplace_back(_seminar);
    }
    std::for_each(data.begin(), data.end(), [&](const nlohmann::json &j) {
        auto new_seminar = j.get<seminar>();
        if (new_seminar.id != _seminar.id) {
            std::cout << "ERROR!!!" << std::endl;
        }
    });
}

void test_protobuf(const seminar &_seminar) {
    Seminar pb_seminar;
    pb_seminar.set_id(_seminar.id);
    pb_seminar.set_name(_seminar.name);
    pb_seminar.set_num_participants(_seminar.num_participants);
    pb_seminar.set_summer_term(_seminar.summer_term);

    std::vector<char *> data;
    data.reserve(iterations);

    auto serialized_size = pb_seminar.ByteSizeLong();

    for (auto i = 0; i < iterations; i++) {
        char *buffer = new char[serialized_size];
        pb_seminar.SerializeToArray(buffer, serialized_size);
        data.push_back(buffer);
    }
    std::for_each(data.begin(), data.end(), [&](const char *pb) {
        Seminar new_seminar;
        new_seminar.ParseFromArray(pb, serialized_size);
        if (new_seminar.id() != _seminar.id) {
            std::cout << "ERROR!!!" << std::endl;
        }
    });
}

int main() {
    GOOGLE_PROTOBUF_VERIFY_VERSION;
    std::cout << "Compiled with " << __VERSION__ << '\n';
    std::cout << "Initializing data...\n";

    seminar seminar;
    seminar.id = 55324;
    seminar.name = "Internet-scale Distributed Systems";
    seminar.num_participants = 423;
    seminar.summer_term = true;

    auto start = std::chrono::system_clock::now();
    test_raw(seminar);
    auto end = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_ms = end - start;
    std::cout << "Raw: " << elapsed_ms.count() << "s\n";

    start = std::chrono::system_clock::now();
    test_json(seminar);
    end = std::chrono::system_clock::now();

    elapsed_ms = end - start;
    std::cout << "JSON: " << elapsed_ms.count() << "s\n";

    start = std::chrono::system_clock::now();
    test_protobuf(seminar);
    end = std::chrono::system_clock::now();

    elapsed_ms = end - start;
    std::cout << "Protocol Buffers: " << elapsed_ms.count() << "s\n";


    return 0;
}
