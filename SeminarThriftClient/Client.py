#!/usr/bin/env python

import sys
sys.path.append('../../gen-py')

from seminar import SeminarManager
from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol


try:
    transport = TSocket.TSocket('localhost', 8080)

    transport = TTransport.TBufferedTransport(transport)

    protocol = TBinaryProtocol.TBinaryProtocol(transport)

    client = SeminarManager.Client(protocol)

    seminar = SeminarManager.Seminar()
    seminar.id = 2131
    seminar.name = "Internet Distributed Systems"

    participant = SeminarManager.Participant()
    participant.id = 34324
    participant.typ = SeminarManager.Type.STUDENT

    transport.open()

    client.addParticipantToSeminar(seminar, participant)
    seminar = client.getSeminarByName(seminar.name)

    print('Number of Participants: %i' % len(seminar.participants))

except Thrift.TException as tx:
    print('%s' % tx.message)
